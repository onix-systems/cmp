<?php

use yii\db\Migration;

class m160314_122417_CreateISO3166_1Table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%iso3166_1}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'alfa2_code' => $this->string(2),
            'alfa3_code' => $this->string(3),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%iso3166_1}}');
    }
}
